//
//  ChatViewController.swift
//  WhatAppTry1
//
//  Created by sachin yadav on 17/03/17.
//  Copyright © 2017 sachin yadav. All rights reserved.
//

import UIKit

class ChatViewController: UITableViewController {
    
        let id = "reuseIdentifier"
        let headerId = "reuseid"
        var arr = [String]()
        
    
        override func viewDidLoad() {
            super.viewDidLoad()
            
            self.title = "Chats"
            tableView.register(ChatCell.self, forCellReuseIdentifier: id)
            tableView.rowHeight = 75.0
            arr = ["Sachin","Papa","Mummy","Sachin","Papa","Mummy","Sachin","Papa","Mummy"]
            setupNav()
        }
        
        
        // tableview code
        override func numberOfSections(in tableView: UITableView) -> Int {
            // #warning Incomplete implementation, return the number of sections
            return 1
        }
        
        override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arr.count
        }
        
        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: id, for: indexPath) as! ChatCell
            cell1.accessoryType = .disclosureIndicator
            return cell1
        }
        
        override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
                   return true
        }
        
        override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
            
            if editingStyle == .delete {
                arr.remove(at: indexPath.row)
                print("Delete")
                tableView.deleteRows(at: [indexPath], with: .fade)
                tableView.reloadData()
            }
        }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 4.0
        layout.scrollDirection = .vertical
        let chatvc = IndivisualChat(collectionViewLayout: layout )
        navigationController?.pushViewController(chatvc, animated: true)
    }
        override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
            
                tableView.separatorInset =  UIEdgeInsetsMake(0, 70, 0, 0)
    }
    
    // trying new things

    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let editAction = UITableViewRowAction(style: .normal, title: "Edit") { (rowAction, indexPath) in
            //TODO: edit the row at indexPath here
        }
        editAction.backgroundColor = .blue
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (rowAction, indexPath) in
            //TODO: Delete the row at indexPath here
        }
        deleteAction.backgroundColor = .red
        
        return [editAction,deleteAction]
    }
    
        func ClearBtnAction() {
            arr = []
            tableView.reloadData()
        }
        
        func EditAction() {
            
            tableView.isEditing = !tableView.isEditing
            
            if tableView.isEditing == true {
                self.navigationItem.leftBarButtonItem?.title = "done"
                self.navigationItem.leftBarButtonItem?.action = #selector(self.EditAction)
                self.navigationItem.rightBarButtonItem =  nil
                tableView.reloadData()
            }
            else {
                self.navigationItem.leftBarButtonItem?.title = "Edit"
                let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "createNew"), style: .plain, target: self, action: #selector(self.ClearBtnAction))
                self.navigationItem.rightBarButtonItem = button1
                tableView.reloadData()
            }
        }
        
        func setupNav(){
            let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "createNew"), style: .plain, target: self, action: #selector(self.ClearBtnAction))
            self.navigationItem.rightBarButtonItem = button1
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action:#selector(self.EditAction))
            self.navigationController?.navigationBar.isTranslucent = false

        }
    
    
    
    }


class ChatCell:UITableViewCell {
    
    
    let ProfileImg = { () -> UIImageView in
        let img =  UIImageView()
        img.clipsToBounds = true
        img.layer.cornerRadius = 25.0
        img.backgroundColor = .red
        img.image = #imageLiteral(resourceName: "Sachin")
        return img
    }()
        let NameLabel = { () -> UILabel in
        let lab = UILabel()
        lab.text = "Sachin Yadav"
        lab.font = UIFont.systemFont(ofSize: 16.0)
        lab.textColor = UIColor.darkText
        return lab
    }()
    let TimeLabel = { () -> UILabel in
        let lab = UILabel()
        lab.text = "4:55 PM"
        lab.font = UIFont.systemFont(ofSize: 12.0)
        lab.textColor = UIColor.lightGray
        return lab
    }()
    let LastMessage = { () -> UILabel in
        let lab = UILabel()
        lab.text = "last message "
        lab.font = UIFont.systemFont(ofSize: 14.0)
        lab.textColor = UIColor.lightGray


        return lab
    }()

    
    func Setup() {
        
        self.contentView.addSubview(NameLabel)
        self.contentView.addSubview(TimeLabel)
        self.contentView.addSubview(NameLabel)
        self.contentView.addSubview(ProfileImg)
        self.contentView.addSubview(LastMessage)

        
        ProfileImg.anchor(nil, left: self.contentView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        ProfileImg.anchorCenterYToSuperview()
        
        NameLabel.anchor(ProfileImg.topAnchor, left: ProfileImg.rightAnchor, bottom: nil, right: nil, topConstant: 4, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
         LastMessage.anchor(nil, left: ProfileImg.rightAnchor, bottom: ProfileImg.bottomAnchor, right: nil, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
         TimeLabel.anchor(NameLabel.topAnchor, left: nil, bottom: nil, right: self.contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 2, widthConstant: 0, heightConstant: 0)
  
    }
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
         Setup()
    }
    ///new stuff trying
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

