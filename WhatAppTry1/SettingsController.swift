//
//  SettingsController.swift
//  WhatAppTry1
//
//  Created by sachin yadav on 17/02/17.
//  Copyright © 2017 sachin yadav. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"
private let reuseIdentifierA = "Cell1"


class SettingsController: UICollectionViewController , UICollectionViewDelegateFlowLayout{

    let tittleArray = [["Starred Messages","WhatsApp Web/Desktop"],["Account","Chats","Notifications","Data and Storage Usage"],["About and Help","Tell a Friend"]]
    let tittleimage = [[#imageLiteral(resourceName: "star"),#imageLiteral(resourceName: "desktop")],[#imageLiteral(resourceName: "key"),#imageLiteral(resourceName: "chats"),#imageLiteral(resourceName: "notifications"),#imageLiteral(resourceName: "data")],[#imageLiteral(resourceName: "info-1"),#imageLiteral(resourceName: "love")]]
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView?.backgroundColor = UIColor(colorLiteralRed: 240/255, green: 239/255, blue: 224/255, alpha: 1.0)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(cellA.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView!.register(cellB.self, forCellWithReuseIdentifier:reuseIdentifierA)

        self.title = "Settings"
        self.navigationController?.navigationBar.isTranslucent = false

    }

    override init(collectionViewLayout layout: UICollectionViewLayout) {
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 5
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(section)
        switch section {
        case 0:
            return 1
            
        case 1:
            return 2
            
        case 2:
            return 4
            
        case 3:
            return 2
            
        case 4:
            return 0
            


        default:
            return 0
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! cellA
            
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierA, for: indexPath) as! cellB
           
            cell.TittleLabel.text = tittleArray[indexPath.section - 1][indexPath.row]

            cell.TitteImg.image = tittleimage[indexPath.section - 1][indexPath.row]
            
            
        if tittleArray[indexPath.section - 1].count != indexPath.row + 1 {
            
            cell.SepratorImg.anchor(nil, left: cell.TittleLabel.leftAnchor, bottom: cell.bottomAnchor, right: cell.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
            }
            
            
            return cell

        }
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.section == 0 {
            return CGSize(width: UIScreen.main.bounds.width, height: 80)
 
        }else{
            return CGSize(width: UIScreen.main.bounds.width, height: 44)

        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 35, left: 0, bottom: 0, right: 0)
    }
    
    
    
    

}





class cellA:UICollectionViewCell {
    
   
    
    override var isHighlighted: Bool {
        didSet {
            if self.isHighlighted == true {
                backgroundColor = UIColor(colorLiteralRed: 222/255, green: 222/255, blue: 222/255, alpha: 1.0)
            }else{
                backgroundColor = .white
            }
        }
    }
    
   
    let ProfileImgView = { () -> UIImageView in 
        let img = UIImageView()
        img.backgroundColor = .red
        img.image = #imageLiteral(resourceName: "Sachin")
        img.contentMode = .scaleAspectFit
        return img
    }()
    
    let NameLabel = { () -> UILabel in 
        let label = UILabel()
        label.font = UIFont(name: "helvetica", size: 19.0)
        label.textAlignment = .left
        label.text = "Sachin"
        return label
    }()
    
    let StatusLabel = { () -> UILabel in
        let label = UILabel()
        label.font = UIFont(name: "helvetica-light", size: 14.0)
        label.textAlignment = .left
        label.text = "Home Sweet home "
        return label
    }()
    
    let ArrowView = { () -> UIImageView in 
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "arrow")
        img.contentMode = .scaleAspectFill
        return img

    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        setupView()
        ProfileImgView.layer.cornerRadius = 32.0
        ProfileImgView.clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setupView (){
        
        addSubview(ProfileImgView)
        addSubview(NameLabel)
        addSubview(StatusLabel)
        addSubview(ArrowView)
        
        ProfileImgView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 8.0, leftConstant: 12.0, bottomConstant: 0, rightConstant: 0, widthConstant: 64, heightConstant: 64)
        NameLabel.anchor(ProfileImgView.topAnchor, left: ProfileImgView.rightAnchor, bottom: nil, right: rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        StatusLabel.anchor(NameLabel.bottomAnchor, left: ProfileImgView.rightAnchor, bottom: nil, right: rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        ArrowView.anchorCenterYToSuperview(constant: 0)
        ArrowView.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 12, widthConstant: 15, heightConstant: 15)
    }
    
    
}



class cellB:UICollectionViewCell {
    
    override var isHighlighted: Bool {
        didSet {
            if self.isHighlighted == true {
                backgroundColor = UIColor(colorLiteralRed: 222/255, green: 222/255, blue: 222/255, alpha: 1.0)
            }else{
                backgroundColor = .white
            }
        }
    }
    
    
    let ArrowView = { () -> UIImageView in
        let img = UIImageView()
        img.backgroundColor = .clear
        img.contentMode = .scaleAspectFill
        img.image = #imageLiteral(resourceName: "arrow")
        return img
        
    }()

    let TitteImg = { () -> UIImageView in
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        return img
    }()
    
    let TittleLabel = { () -> UILabel in
        let label = UILabel()
        label.font = UIFont(name: "helvetica", size: 14.0)
        label.textAlignment = .left
        label.text = "hellllllllllllllll"
        return label
    }()
    
    let SepratorImg = { () -> UIImageView in
        let img = UIImageView()
        img.backgroundColor = .black
        img.contentMode = .scaleAspectFit
        return img
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white

        setupView()
//        TitteImg.layer.cornerRadius = 3.0
        TitteImg.clipsToBounds = true

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setupView (){
        
        addSubview(ArrowView)
        addSubview(TitteImg)
        addSubview(TittleLabel)
        addSubview(SepratorImg)

        ArrowView.anchorCenterYToSuperview(constant: 0)
        ArrowView.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 12, widthConstant: 15, heightConstant: 15)
        TitteImg.anchor(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 30.0, heightConstant: 30.0)
        TitteImg.anchorCenterYToSuperview(constant: 0)
        TittleLabel.anchor(nil, left: TitteImg.rightAnchor, bottom: nil, right: ArrowView.leftAnchor, topConstant: 0, leftConstant: 12.0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        TittleLabel.anchorCenterYToSuperview(constant: 0)



    }
    
    
}

