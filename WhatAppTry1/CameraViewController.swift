//
//  CameraViewController.swift
//  WhatAppTry1
//
//  Created by sachin yadav on 24/02/17.
//  Copyright © 2017 sachin yadav. All rights reserved.
//

import UIKit
import AVFoundation

class CameraViewController: UIViewController {

    var session: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var previewView:UIView!
    var captureImageView:UIImageView!
    
    let exitBtn = { () -> UIButton in 
        let btn = UIButton()
        btn.setTitle("Exit", for: .normal)
        btn.addTarget(self, action: #selector(exit), for: .touchUpInside)
        return btn
        
        
    }()
    
    let FlashBtn = { () -> UIButton in
        let btn = UIButton()
        btn.setTitle("on", for: .normal)
        btn.addTarget(self, action: #selector(exit), for: .touchUpInside)
        return btn
        
        
    }()
    
    let gallery = { () -> UIButton in
        let btn = UIButton()
        btn.setTitle("on", for: .normal)
        btn.addTarget(self, action: #selector(exit), for: .touchUpInside)
        return btn
        
        
    }()

    let ChangeCamera = { () -> UIButton in
        let btn = UIButton()
        btn.setTitle("on", for: .normal)
        btn.addTarget(self, action: #selector(exit), for: .touchUpInside)
        return btn
        
        
    }()


    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        previewView = UIView(frame: UIScreen.main.bounds)
        view.addSubview(previewView)
        SetupSession()
        setup()
            
        }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        videoPreviewLayer!.frame = previewView.bounds
    }
    
    func setup(){
        
        
        previewView.addSubview(exitBtn)
        previewView.addSubview(FlashBtn)
        previewView.addSubview(gallery)
        previewView.addSubview(ChangeCamera)
        
        exitBtn.anchor(previewView.topAnchor , left: previewView.leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        FlashBtn.anchor(previewView.topAnchor , left: nil, bottom: nil, right: previewView.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 20, widthConstant: 50, heightConstant: 50)
        ChangeCamera.anchor(nil , left: nil, bottom: previewView.bottomAnchor, right: previewView.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 20, rightConstant: 20, widthConstant: 50, heightConstant: 50)
        gallery.anchor(nil , left: previewView.leftAnchor, bottom: previewView.bottomAnchor, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 20, rightConstant: 20, widthConstant: 50, heightConstant: 50)

        
    }
    
    
    
    func SetupSession() {
        session = AVCaptureSession()
        session?.sessionPreset = AVCaptureSessionPresetHigh
        let backCamera = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: backCamera)
        } catch let error1 as NSError {
            error = error1
            input = nil
            print(error!.localizedDescription)
        }
        
        if error == nil && session!.canAddInput(input) {
            session!.addInput(input)
            // ...
            // The remainder of the session setup will go here...
        }
        
        stillImageOutput = AVCaptureStillImageOutput()
        stillImageOutput?.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]

        
        if session!.canAddOutput(stillImageOutput) {
            session!.addOutput(stillImageOutput)
            // ...
            // Configure the Live Preview here...
        }
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
        videoPreviewLayer!.videoGravity = AVLayerVideoGravityResizeAspect
        videoPreviewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        previewView.layer.addSublayer(videoPreviewLayer!)
        session!.startRunning()
    }
    
    func exit(){
        
       let vc = MainTabBarController()
        
        self.present(vc, animated: false) {
            print("Welcome")
        }
    }
    
    
    

    
}
