//
//  MainTabBarController.swift
//  WhatAppTry1
//
//  Created by sachin yadav on 16/02/17.
//  Copyright © 2017 sachin yadav. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {

    
    
    let layout: UICollectionViewFlowLayout = {
        let lay = UICollectionViewFlowLayout()
        lay.minimumLineSpacing = 0.0
        lay.minimumInteritemSpacing = 0.0
        lay.scrollDirection = .vertical
        return lay
    }()
    



    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
       }

   func setup()
   {
    
    
    let controller1 = UIViewController()
    let controller2 = UINavigationController(rootViewController: CallsTabController())
    let controller3 = CameraViewController()
    let controller4 = UINavigationController(rootViewController: ChatViewController())
    let controller5 = UINavigationController(rootViewController: SettingsController(collectionViewLayout: layout) )

    
    let bar1 = UITabBarItem(title: "Favorites", image: #imageLiteral(resourceName: "Favorites").withRenderingMode(.automatic), selectedImage: #imageLiteral(resourceName: "FavriateSelected"))
    let bar2 = UITabBarItem(title: "Calls", image: #imageLiteral(resourceName: "Calls"), selectedImage: #imageLiteral(resourceName: "CallFilled"))
    let bar3 = UITabBarItem(title: "Camera", image: #imageLiteral(resourceName: "Contacts"), selectedImage: #imageLiteral(resourceName: "Contacts Filled"))
    let bar4 = UITabBarItem(title: "Chats", image: #imageLiteral(resourceName: "Chat"), selectedImage: #imageLiteral(resourceName: "chatfilled"))
    let bar5 = UITabBarItem(title: "Settings", image: #imageLiteral(resourceName: "Settings"), selectedImage: #imageLiteral(resourceName: "Settings Filled"))

    controller1.tabBarItem = bar1
    controller2.tabBarItem = bar2
    controller3.tabBarItem = bar3
    controller4.tabBarItem = bar4
    controller5.tabBarItem = bar5
    
    self.viewControllers = [controller1,controller2,controller3,controller4,controller5]
    

    
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
