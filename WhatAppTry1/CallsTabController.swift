//
//  CallsTabController.swift
//  WhatAppTry1
//
//  Created by sachin yadav on 21/02/17.
//  Copyright © 2017 sachin yadav. All rights reserved.
//

import UIKit

class CallsTabController: UITableViewController {

    
    let id = "reuseIdentifier"
    var arr = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(cell.self, forCellReuseIdentifier: id)
        arr = ["Sachin","Papa","Mummy","Sachin","Papa","Mummy","Sachin","Papa","Mummy"]
        setupNav()
    }
    
    
// tableview code
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)as! cell
        cell1.accessoryType = .detailButton
    
        cell1.data = arr[indexPath.row]
        
        return cell1
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
     return true
        
     }

     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
     if editingStyle == .delete {
        arr.remove(at: indexPath.row)
        print("Delete")
        tableView.deleteRows(at: [indexPath], with: .fade)
        tableView.reloadData()
     }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        
        let mycell = cell as! cell
        tableView.separatorInset =  UIEdgeInsetsMake(0, 30, 0, 0)



        if tableView.isEditing == true {

            mycell.separatorInset =  UIEdgeInsetsMake(0, 70, 0, 0)
            

                //=  UIEdgeInsetsMake(0, 30, 0, 0)
        }
        else{

            mycell.separatorInset =  UIEdgeInsetsMake(0, 30, 0, 0)


        }
    }
 
    
    func ClearBtnAction() {
        arr = []
        tableView.reloadData()
    }
    
    func EditAction() {
        
        
          tableView.isEditing = !tableView.isEditing

        
        if tableView.isEditing == true {
            self.navigationItem.leftBarButtonItem?.title = "Done"
            self.navigationItem.leftBarButtonItem?.action = #selector(self.EditAction)
            self.navigationItem.rightBarButtonItem =  UIBarButtonItem(title: "Clear", style: .plain, target: self, action:#selector(self.ClearBtnAction))
            tableView.reloadData()

            
        }
        else {
            
            self.navigationItem.leftBarButtonItem?.title = "Edit"
            let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "Calls"), style: .plain, target: self, action: #selector(self.ClearBtnAction))
            self.navigationItem.rightBarButtonItem = button1
            tableView.reloadData()

        }
        
    }

    func setupNav(){
        
        let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "Calls"), style: .plain, target: self, action: #selector(self.ClearBtnAction))
        
        self.navigationItem.rightBarButtonItem = button1
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action:#selector(self.EditAction))
        
        self.navigationItem.titleView = {
            let search = UISegmentedControl(items: ["All","Missed"])
            search.apportionsSegmentWidthsByContent = false
            search.selectedSegmentIndex = 0
            return search
        }()
        self.tableView.addSubview(UISearchBar())
        
        
    }
}










class cell : UITableViewCell {
    
    
    
    
    let frameview = { () -> UIView in 
        let view = UIView()
        return view
    }()
    
    var Name = { () -> UILabel in
        let lab = UILabel()
        lab.font = UIFont(name: "san-francisco-Light", size: 16.0)
        lab.textColor = UIColor.black

        return lab
    }()
    var Description = { () -> UILabel in
        let lab = UILabel()
        lab.font = UIFont(name: "helveticaNeue-UltraLight", size: 12.0)
        lab.textColor = UIColor.black
        return lab
    }()
    
    var TimeLabel = { () -> UILabel in
        let lab = UILabel()
        lab.textAlignment = .right
        lab.font = UIFont(name: "helveticaNeue-light", size: 12.0)
        lab.textColor = UIColor.lightGray
        lab.text = "yesterday"
        return lab
    }()

    var Calltype:UIImageView = { () -> UIImageView in
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image =  #imageLiteral(resourceName: "Recivecall")
        return imageView
    }()

    var  data:String? {
        didSet {
            Name.text = self.data
            Description.text = "home"
        }
    }
    
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        Setup()
       Ancher()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func Setup() {
        
        self.contentView.addSubview(frameview)
        self.contentView.addSubview(Calltype)
        self.contentView.addSubview(Name)
        self.contentView.addSubview(Description)
        self.contentView.addSubview(TimeLabel)
      
    }
    
    func Ancher() {
        Calltype.anchor(frameview.topAnchor, left: frameview.leftAnchor, bottom: frameview.bottomAnchor, right: nil, topConstant: 8, leftConstant: 8, bottomConstant: 8, rightConstant: 0, widthConstant: 20.0, heightConstant: 0)
        
        Description.anchor(nil, left: Calltype.rightAnchor, bottom: frameview.bottomAnchor, right: TimeLabel.leftAnchor, topConstant: 1, leftConstant: 8, bottomConstant: 4, rightConstant: 4, widthConstant: 0, heightConstant: 0)
        
        Name.anchor(frameview.topAnchor, left: Calltype.rightAnchor, bottom: nil, right: TimeLabel.leftAnchor, topConstant: 2, leftConstant: 8, bottomConstant: 0, rightConstant: 4, widthConstant: 0, heightConstant: 0)
        
        TimeLabel.anchor(nil, left: nil, bottom: nil, right: self.contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        TimeLabel.anchorCenterYToSuperview()
        
        frameview.anchor(self.contentView.topAnchor, left: self.contentView.leftAnchor, bottom: self.contentView.bottomAnchor, right: self.contentView.rightAnchor, topConstant: 0, leftConstant: 0.0, bottomConstant: 0, rightConstant:  0, widthConstant: 0, heightConstant: 0)

    }

}
